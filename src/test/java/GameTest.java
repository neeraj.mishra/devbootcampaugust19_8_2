import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.PrintStream;
import java.util.Scanner;

import static org.mockito.Mockito.*;


public class GameTest {
    private Game game;
    private Player player1;
    private Player player2;
    private Machine machine;
    private PrintStream console;
    @Before
    public void setup(){

         player1 = mock(Player.class);
         player2 = mock(Player.class);
         machine = mock(Machine.class);
         console = mock(PrintStream.class);
    }
    @Test
    public void shouldPlayGameForOneRound(){
        // Intial setup
        game = new Game(player1, player2, 1, machine, console);


        String player1input = "player1input";
        String player2input = "player2input";
        when(player1.getMove()).thenReturn(player1input);
        when(player2.getMove()).thenReturn(player2input);
        Score score = Score.getScore(100, 200);
        when(machine.calculateScore(player1input, player2input)).thenReturn(score);

        when(player1.getScore()).thenReturn(22);
        when(player2.getScore()).thenReturn(33);

        game.play();

        verify(player1).getMove();
        verify(player2).getMove();
        verify(machine).calculateScore(player1input, player2input);

        verify(player1).updateScore(score.playerOneScore);
        verify(player2).updateScore(score.playerTwoScore);

        verify(console).println("Score 22 33");

    }
    @Test
    public void shouldPlayGameForTwoRounds(){
        Score scoreForFirstRound = Score.getScore(100, 200);
        Score scoreForSecondRound = Score.getScore(300, 0);
        game = new Game(player1, player2, 2, machine, console);

        when(player1.getMove()).thenReturn("player1input").thenReturn("player1input");
        when(player2.getMove()).thenReturn("player2input").thenReturn("player2input");
        when(machine.calculateScore("player1input", "player2input")).thenReturn(scoreForFirstRound).thenReturn(scoreForSecondRound);

        when(player1.getScore()).thenReturn(22).thenReturn(44);
        when(player2.getScore()).thenReturn(33).thenReturn(11);

        game.play();

        verify(player1, times(2)).getMove();
        verify(player2, times(2)).getMove();
        verify(machine, times(2)).calculateScore("player1input", "player2input");

        verify(player1).updateScore(scoreForFirstRound.playerOneScore);
        verify(player2).updateScore(scoreForFirstRound.playerTwoScore);
        verify(player1).updateScore(scoreForSecondRound.playerOneScore);
        verify(player2).updateScore(scoreForSecondRound.playerTwoScore);

        verify(console).println("Score 22 33");
        verify(console).println("Score 44 11");
    }
}
