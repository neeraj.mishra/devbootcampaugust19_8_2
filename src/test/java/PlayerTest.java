import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Scanner;

public class PlayerTest {

    static Player player;

    @Test
    public  void shouldIntialization(){
        Player player = new Player();
    }

    @Test
    @Before
    public void shouldInitializeParameterizedConstructor(){
        player = new Player("Player1");
        Assert.assertEquals(player.getName(),"Player1");
        Assert.assertEquals(player.getScore(), 0);
    }

    @Test
    public void shouldUpdateScore() {
        this.player.setScore(0);
        this.player.updateScore(-1);
        Assert.assertEquals(-1, this.player.getScore());
    }
    @Test
    public void shouldGetMove(){
        MoveBehavior moveBehavior = () -> new Scanner("c").next();
        Player player1 = new Player(moveBehavior);
        String move = player1.getMove();
        Assert.assertEquals("c", move);

    }

    @Test
    public void shouldReturnOnlyCheatPlayerMove(){
        MoveBehavior moveBehavior = () -> "c";
        Assert.assertEquals("c",moveBehavior.getMove());
    }
    @Test
    public void shouldReturnOnlyCooperatePlayerMove(){
        MoveBehavior moveBehavior = () -> "nc";
        Assert.assertEquals("nc",moveBehavior.getMove());
    }

    @Test
    public void shouldReturnManualPlayerMove() {
        MoveBehavior moveBehavior = () -> new Scanner("c").next();
        Assert.assertEquals("c", moveBehavior.getMove());
    }
}
