import org.junit.Test;

import java.io.PrintStream;

import static org.mockito.Mockito.verify;

public class GameIntegrationTest {
    @Test
    public void shouldReturnForCheaterVsCooperate(){
        Player playerAlwaysCheat = new Player(()-> "nc");
        Player playerAlwaysCooperate = new Player(()-> "c");
        PrintStream console = System.out;
        Game game = new Game(playerAlwaysCheat, playerAlwaysCooperate, 5, new Machine(), console);
        game.play();
        verify(console).print("Score 3 -1");

        verify(console).print("Score 6 -2");

        verify(console).print("Score 9 -3");
        verify(console).print("Score 12 -4");
        verify(console).print("Score 15 -5");
    }
}
