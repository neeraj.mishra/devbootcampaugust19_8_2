import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class MachineTests{
    static public String coin = "c";
    static public String noCoin = "nc";
    static public String invalidEntry = "dasd2213";
    static public Machine machine;
    @Test
    @Before
    public void shouldInitializeMachine(){
        machine = new Machine();
    }

    @Test
    public void shouldValidatePlayerMove() {
        Assert.assertEquals(machine.validatePlayerMove(coin), true);
        Assert.assertEquals(machine.validatePlayerMove(invalidEntry), false);
        Assert.assertEquals(machine.validatePlayerMove(noCoin), true);
    }

    @Test
    public void shouldValidatePlayerWithInvalidMove() {
        Assert.assertEquals(machine.validatePlayerMove(invalidEntry), false);
    }

    @Test
    public void shouldCalculateScore() {
        Assert.assertEquals(machine.calculateScore(coin, coin), Score.getScore(2, 2));
        Assert.assertEquals(machine.calculateScore(coin, noCoin), Score.getScore(-1, 3));
        Assert.assertEquals(machine.calculateScore(noCoin, coin), Score.getScore(3, -1));
        Assert.assertEquals(machine.calculateScore(noCoin, noCoin), Score.getScore(0, 0));
    }


}