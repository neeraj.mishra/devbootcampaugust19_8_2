import java.util.Objects;

public class Score {

    public int playerOneScore;
    public int playerTwoScore;

    static Score getScore(int playerOneScore, int playerTwoScore) {
        Score score = new Score();
        score.playerOneScore = playerOneScore;
        score.playerTwoScore = playerTwoScore;
        return score;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Score score = (Score) o;
        return playerOneScore == score.playerOneScore &&
                playerTwoScore == score.playerTwoScore;
    }

    @Override
    public int hashCode() {
        return Objects.hash(playerOneScore, playerTwoScore);
    }
}
