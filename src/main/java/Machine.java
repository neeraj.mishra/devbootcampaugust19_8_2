public class Machine {
    static final public String cooperateMove = "c";
    static final public String cheatMove = "nc";
    public boolean validatePlayerMove(String coin) {
        if(coin.equals(cooperateMove) || coin.equals(cheatMove))
            return true;
        return false;
    }


    public Score calculateScore(String player1Input, String player2Input) {
        if(player1Input.equals(cooperateMove) && player2Input.equals(cheatMove)){

            return Score.getScore(-1,3);
        }
        else if(player1Input.equals(cheatMove) && player2Input.equals(cooperateMove)){

            return Score.getScore(3,-1);
        }
        else if(player1Input.equals(cooperateMove) && player2Input.equals(cooperateMove)){

            return Score.getScore(2,2);
        }
        return Score.getScore(0,0);
    }
}
