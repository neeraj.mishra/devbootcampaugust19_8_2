import java.util.Scanner;

public class Player {

    private String name;
    private int score;
    private MoveBehavior moveBehavior;

    Player(){
        this.name = "NA";
        this.score = 0;
    }

    Player(String name){
        this.name = name;
        this.score = 0;
    }
    Player(MoveBehavior moveBehavior){
        this.moveBehavior = moveBehavior;
    }

    public int getScore() {
        return score;
    }

    public String getName() {
        return name;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public void updateScore(int score) {
        this.score += score;
    }

    public String getMove() {
        return moveBehavior.getMove();
    }
}



