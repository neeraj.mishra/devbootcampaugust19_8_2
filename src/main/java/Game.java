import java.io.PrintStream;
import java.util.Scanner;

public class Game {
    Player player1;
    Player player2;
    int noOfRounds;
    Machine machine;
    PrintStream console;

    public Game(Player player1, Player player2, int noOfRounds, Machine machine, PrintStream console) {
        this.player1 = player1;
        this.player2 = player2;
        this.noOfRounds = noOfRounds;
        this.machine = machine;
        this.console = console;
    }


    public void play() {
        for(int i = 0; i < this.noOfRounds; i++) {
            Score score = machine.calculateScore(player1.getMove(), player2.getMove());

            player1.updateScore(score.playerOneScore);
            player2.updateScore(score.playerTwoScore);

            console.println("Score " + player1.getScore() + " " + player2.getScore());
        }
    }
}
